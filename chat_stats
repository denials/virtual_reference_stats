#!/usr/bin/env python

"""
Analyze results from Ask the Library

* Operator stats query: "operator:*lan* from:2019-07-01 to:2020-07-01"
* Queue stats query: "queue:laurentian* from:2019-07-01 to:2020-07-01"

"""

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

pd.set_option("display.max_rows", 500)

plt.close("all")

# Load in the operator stats
with open("operators.csv", "r") as src:
    operators = pd.read_csv(src)

dropcols = [
    "id",
    "guest",
    "protocol",
    "wait",
    "ip",
    "referrer",
]

operators.drop(dropcols, axis="columns", inplace=True)

nonlu = [
    "alana_car",
    "alanna_west",
    "ilana_gue",
    "melanie_ott",
    "melaniec_gue",
]

for name in nonlu:
    operators = operators[lambda df: df.operator != name]

# Use real first names instead of operator names
operators.replace(
    {
        "alain_lan.fr": "Alain",
        "ashley_lan": "Ashley",
        "dan_lan": "Dan",
        "desmond_lan.fr": "Desmond",
        "jenniferr_lan.fr": "Jennifer",
        "leila_lan.fr": "Leïla",
        "tomasz_lan.fr": "Tomasz",
    },
    inplace=True,
)

# Treat the operators as a categorical variable
operators["operator"] = operators["operator"].astype("category")

# Convert this to datetime instead of treating it like a string
operators["started"] = pd.to_datetime(operators["started"])

# Then truncate the dates to just the month value
operators["month"] = operators["started"].values.astype("datetime64[M]")

print(operators.describe())

print("Laurentian operators")
grp = operators[["operator"]].groupby(["operator"])["operator"].count()
print(grp)
print(grp.describe())


print("Laurentian operators, by month")
grp = operators[["month", "operator"]].groupby(["month", "operator"])["month"].count()
print(grp)
print(grp.describe())

# Unstack the operator so we can restack them in the bar graph
ts = grp.unstack("operator")

fig, ax = plt.subplots(figsize=(15, 7))

slice = ts.plot.bar(stacked=True, ax=ax).legend(bbox_to_anchor=(1, 1))
ax.xaxis.set_major_formatter(
    plt.FixedFormatter(ts.index.to_series().dt.strftime("%Y %b"))
)
ax.set(title="2019-2020 Virtual Reference", ylabel="Chats", xlabel="Month")

fig = slice.get_figure()
fig.savefig("operators.png", format="png")

# And let's save the operators into a TSV file
with (open("processed_vr_operators.tsv", "w")) as outfile:
    operators.to_csv(outfile, sep="\t", index=False)

with (open("queues.csv", "r")) as src:
    queues = pd.read_csv(src)

queues.drop(dropcols, axis="columns", inplace=True)

# Use language instead of queue names
queues.replace(
    {"laurentian": "English", "laurentian-fr": "French",}, inplace=True,
)

# Treat the queues as a categorical variable
queues["queue"] = queues["queue"].astype("category")

# Convert this to datetime instead of treating it like a string
queues["started"] = pd.to_datetime(queues["started"])

# Then truncate the dates to just the month value
queues["month"] = queues["started"].values.astype("datetime64[M]")

print("Laurentian questions answered by queue")
grp = queues[["queue"]].groupby(["queue"])["queue"].count()
print(grp)
print(grp.describe())

print("Laurentian questions answered by month and queue")
grp = queues[["month", "queue"]].groupby(["month", "queue"])["month"].count()
print(grp)
print(grp.describe())

# Unstack the queue so we can restack them in the bar graph
ts = grp.unstack("queue")

fig, ax = plt.subplots(figsize=(15, 7))

slice = ts.plot.bar(stacked=True, ax=ax).legend(bbox_to_anchor=(1, 1))
ax.xaxis.set_major_formatter(
    plt.FixedFormatter(ts.index.to_series().dt.strftime("%Y %b"))
)
ax.set(title="2019-2020 Virtual Reference", ylabel="Chats", xlabel="Month")

fig = slice.get_figure()
fig.savefig("queues.png", format="png")

# And let's save the queues into a TSV file
with (open("processed_queues.tsv", "w")) as outfile:
    queues.to_csv(outfile, sep="\t", index=False)
